export const state = () => ({
  locations: []
})

export const getters = {
  getCities(state) {
    return state.locations;
  }
}

export const mutations = {
  addLocations (state, payload) {
    payload.forEach((location) => state.locations.push(location))
  },
}
